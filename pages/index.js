import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
const FIREBASE_API_KEY = process.env.FIREBASE_API_KEY
const FIREBASE_AUTH_DOMAIN = process.env.FIREBASE_AUTH_DOMAIN
const FIREBASE_PROJECT_ID = process.env.FIREBASE_PROJECT_ID
const FIREBASE_STORAGE_BUCKET = process.env.FIREBASE_STORAGE_BUCKET
const FIREBASE_MESSAGE_SENDER_ID = process.env.FIREBASE_MESSAGE_SENDER_ID
const FIREBASE_APP_ID = process.env.FIREBASE_APP_ID
const FIREBASE_MEASUREMENT_ID = process.env.FIREBASE_MEASUREMENT_ID
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
      {FIREBASE_API_KEY}
      {FIREBASE_AUTH_DOMAIN}
      {FIREBASE_PROJECT_ID}
      {FIREBASE_STORAGE_BUCKET}
      {FIREBASE_MESSAGE_SENDER_ID}
      {FIREBASE_APP_ID}
      {FIREBASE_MEASUREMENT_ID}
         
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
